const fetch = require("node-fetch");

exports.handler = async () => {
  const corgisPromise = fetch(
    `https://no-cors-api.netlify.app/api/corgis`
  ).then((res) => res.json());

  const unsplashPromise = fetch(
    `https://api.unsplash.com/collections/48405776/photos`,
    {
      headers: {
        Authorization: `Client-ID ${process.env.UNSPLASH_ACCESS_KEY}`,
      },
    }
  ).then((res) => res.json());

  let [corgisData, unsplashData] = await Promise.all([
    corgisPromise,
    unsplashPromise,
  ]);

  const corgis = corgisData.map((corgi) => {
    const unsplashMatch = unsplashData.find(
      (unsplashEntry) => unsplashEntry.id === corgi.id
    );
    const { alt_description, urls, user } = unsplashMatch;

    return {
      ...corgi,
      alt: alt_description,
      url: `${urls.raw}&auto=format&fit=crop&w=300&h=300&q=80&crop=entropy`,
      credit: `${user.name}`,
    };
  });

  // console.log("corgis", corgisData);
  // console.log("unsplash", unsplashData);
  console.log(corgis);

  return {
    statusCode: 200,
    body: JSON.stringify(corgis),
  };
};
